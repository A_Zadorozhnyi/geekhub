package lesson_3;

/**
 * Created by Alex_B on 17.11.2014.
 */

    import java.io.BufferedReader;
    import java.io.FileReader;
    import java.io.IOException;
    import java.util.Scanner;

    public class Item2Productivity {

        public static void main(String[] args) throws IOException {

            BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Alex_B\\Downloads\\StringTest.txt"));

            StringBuilder sb = new StringBuilder();
            String line = null;
            while ( (line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            String[] words = sb.toString().split("\\s+");
            System.out.println("Total words:" + words.length);
            waitEnter();

            long startTimeForString = System.currentTimeMillis();
            String buff = "";
            for (String word : words) {
                buff += word + " ";
            }
            long endTimeForString =System.currentTimeMillis();

            long startTimeForStringBuffer = System.currentTimeMillis();
            StringBuffer buffStringBuffer = new StringBuffer();
            for (String word : words) {
                buffStringBuffer.append(word).append(" ");
            }
            long endTimeForStringBuffer =System.currentTimeMillis();

            long startTimeForStringBuilder = System.currentTimeMillis();
            StringBuilder buffStringBuilder = new StringBuilder();
            for (String word : words) {
                buffStringBuilder.append(word).append(" ");
            }
            long endTimeForStringBuilder =System.currentTimeMillis();

            System.out.println("Complete, lenght:" + buff.length());
            System.out.println("Elapsed time for String:" + (endTimeForString - startTimeForString) + "ms");
            System.out.println("Elapsed time for StringBuffer:" + (endTimeForStringBuffer - startTimeForStringBuffer) + "ms");
            System.out.println("Elapsed time for StringBuilder:" + (endTimeForStringBuilder - startTimeForStringBuilder) + "ms");

        }

        private static void waitEnter() {
            Scanner scaner = new Scanner(System.in);
            System.out.print("Press Enter key.");
            scaner.nextLine();
        }


}
