package lesson_3;

import java.util.Scanner;

/**
 * Created by Alex_B on 22.10.2014.
 */
public class Item3Menu {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        Item3Boat boat = new Item3Boat();
        Item3SolarCar solarCar = new Item3SolarCar();
        boolean exit = false;

        while (exit != true){
            System.out.println(" 1 - select boat \n 2 - select solar car \n 0 - exit");
            int selectCar = scanner.nextInt();
            switch (selectCar) {
                case 1:
                    boat.functional();
                    break;
                case 2:
                    solarCar.functional();
                    break;
                case 0:
                    exit = boat.brake();
                    break;
                default:
                    System.out.println("Error!");
            }
        }
    }

}
