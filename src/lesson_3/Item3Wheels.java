package lesson_3;

/**
 * Created by Alex_B on 28.10.2014.
 */
public class Item3Wheels {

    public boolean wheels(int counterWheels) {

        if (counterWheels > 0) {
            return true;
        } else {
            System.out.println("Error! You need to change a wheel");
            return false;
        }
    }

}
