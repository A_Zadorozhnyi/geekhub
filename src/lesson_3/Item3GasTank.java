package lesson_3;

/**
 * Created by Alex_B on 28.10.2014.
 */
public class Item3GasTank {


    public boolean gasTank(double fuel) {
        if (fuel > 0) {
            return true;
        } else {
            System.out.println("Error! In the tank ran out of fuel ");
            return false;
        }
    }

}
