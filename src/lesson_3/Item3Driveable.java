package lesson_3;

/**
 * Created by Alex_B on 22.10.2014.
 */
public interface Item3Driveable {

    void accelerate();

    boolean brake();

    void turn();

}
