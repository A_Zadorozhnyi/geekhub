package lesson_3;
import java.util.TreeSet;
/**
 * Created by Alex_B on 17.11.2014.
 */

    public class Item1Main {

        public static void main(String[] args) {
            TreeSet<Item1Person> ex = new TreeSet<Item1Person>();
            ex.add(new Item1Person("Smirnov", 23));
            ex.add(new Item1Person("Andreev", 12));
            ex.add(new Item1Person("Petrov", 64));
            ex.add(new Item1Person("Esenina", 13));
            ex.add(new Item1Person("Smirnova", 86));

            for(Item1Person person : ex) {
                System.out.println("Last name: " + person.lastName + " | Age: " + person.age);
            }
        }

    }

