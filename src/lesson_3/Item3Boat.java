package lesson_3;

import java.util.Scanner;

/**
 * Created by A_Zadorozhnyi on 23.10.2014.
 */
public class Item3Boat extends Item3Vehicle {

    Item3Engine engine = new Item3Engine();
    Scanner scanner = new Scanner(System.in);
    String errorTankCapacity = "Error, tank capacity is 20 liters";
    String menuOfPlesureBoat = "1 - accelerate. \n2 - reduce speed \n3 - turn. " +
            "\n4 - check the amount of fuel. \n5 - add fuel. \n6 - default information about the vehicle" +
            "\n111 - info \n0 - return. ";
    int counterOfMaxSpeed = 1;
    boolean exitBoat = false;
    double fuel = 20;
    int counterWheels = 1;

    public double gasTank(double fuel) {
        return fuel;
    }

    public void opportunityToSwim() {
        System.out.println("On this vehicle, you can swim");
    }

    public void addFuel() {
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("You can even fill " + (20 - fuel) + " liter. ");
        double addFuel = scanner.nextDouble();
        if ((addFuel + fuel) <= 20) {
            fuel += addFuel;
        } else {
            System.out.println(errorTankCapacity);
        }
    }

    public void speedLimit(int counterOfMaxSpeed){
        if (counterOfMaxSpeed <= 110){
            accelerate();
        } else {
            System.out.println("You are driving at maximum speed for this machine. ");
        }
    }

    public void defaultInformationAboutTheBoat(){
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("Volume of the tank = 20 liters. ");
        System.out.println("Fuel consumption  8 liters / 100 km");
        System.out.println("Maximum speed of 110 km/h");
    }

    public void info(){
        System.out.println("4 - show remaining fuel");
        System.out.println("7 - travel 100 km.");
        System.out.println("8 - filled 10 liters");
        System.out.println("9 - increased the speed of 10 km / h");
        System.out.println("10 - lowered the speed to 10 km / h");
    }

    public void functional() {
        opportunityToSwim();
        gasTank(fuel);
        do {
            if (engine.engine(counterWheels, fuel) == true) {
                System.out.println(menuOfPlesureBoat);
                int actionOfBoat = scanner.nextInt();
                switch (actionOfBoat) {
                    case 1:
                        counterOfMaxSpeed++;
                        speedLimit(counterOfMaxSpeed);
                        fuel = fuel - 0.08;
                        break;
                    case 2:
                        counterOfMaxSpeed--;
                        speedLimit(counterOfMaxSpeed);
                        fuel = fuel - 0.08;
                        break;
                    case 3:
                        turn();
                        fuel = fuel - 0.08;
                        break;
                    case 4:
                        System.out.println("Fuel: " + gasTank(fuel) + " liters. ");
                        break;
                    case 5:
                        addFuel();
                        break;
                    case 6:
                        defaultInformationAboutTheBoat();
                        break;
                    case 7:
                        fuel = fuel - 8;
                        System.out.println("You drove 100 km. ");
                        break;
                    case 8:
                        fuel = fuel + 10;
                        System.out.println("You have filled 10 liters. ");
                        break;
                    case 9:
                        fuel = fuel - 0.08;
                        counterOfMaxSpeed = counterOfMaxSpeed + 10;
                        System.out.println("You increased the speed of 10 km / h ");
                        speedLimit(counterOfMaxSpeed);
                        break;
                    case 10:
                        fuel = fuel - 0.08;
                        counterOfMaxSpeed = counterOfMaxSpeed - 10;
                        System.out.println("You lowered the speed to 10 km / h ");
                        break;
                    case 0:
                        exitBoat = brake();
                        break;
                    case 111:
                        info();
                        break;
                    default:
                        System.out.println(errorSpelling);
                        break;
                }
            } else {
                addFuel();
            }
        } while (exitBoat != true);
    }

}
