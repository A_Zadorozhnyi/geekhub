package lesson_3;

import java.util.Scanner;

/**
 * Created by A_Zadorozhnyi on 23.10.2014.
 */

/**
 *exception handling
 */
public class Item3SolarCar extends Item3Vehicle {

    Item3Engine engine = new Item3Engine();
    Scanner scanner = new Scanner(System.in);
    String menuOfSolarCar = "1 - accelerate. \n2 - reduce speed \n3 - turn. " +
            "\n4 - check the amount of fuel. \n5 - add fuel. \n6 - default information about the solar car" +
            "\n111 - info \n0 - return. ";
    boolean exitSolarCar = false;
    String errorTankCapacity = "Error, tank capacity is 30 liters";
    int counterOfMaxSpeed = 1;
    int counterWheels = 5;
    double fuel = 16.0;


    public void landTransport() {
        System.out.println("It's a ground transportation");
    }

    public void replaceWheels() {
        System.out.println("Wheels are worn!\n1 - to change them. \n2 - to exit. ");
        int replaceWheels = scanner.nextInt();
        try {
            counterWheels = counterWheels / 1;
            System.out.println("Try block message");
        } catch (ArithmeticException e) {
            System.out.println("Wheels burst and you have an accident.");
            exitSolarCar = brake();
        }
    }

    public void addFuel() {
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("You can even fill " + (30 - fuel) + " liter. ");
        double addFuel = scanner.nextDouble();
        if ((addFuel + fuel) <= 30) {
            fuel += addFuel;
        } else {
            System.out.println(errorTankCapacity);
        }
    }

    public void speedLimit(int counterOfMaxSpeed){
        if (counterOfMaxSpeed <= 110){
            accelerate();
        } else {
            System.out.println("You are driving at maximum speed for this machine. ");
        }
    }

    public void defaultInformationAboutTheSolarCar(){
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("Volume of the tank = 30 liters. ");
        System.out.println("Fuel consumption  8 liters / 100 km");
        System.out.println("Maximum speed of 110 km/h");
    }

    public void info(){
        System.out.println("4 - show remaining fuel");
        System.out.println("7 - travel 100 km.");
        System.out.println("8 - filled 10 liters");
        System.out.println("9 - increased the speed of 10 km / h");
        System.out.println("10 - lowered the speed to 10 km / h");
    }

    public void functional() {
        landTransport();
        do {
            if (engine.engine(counterWheels, fuel) == true) {
                System.out.println(menuOfSolarCar);
                int actionOfSolarCar = scanner.nextInt();
                switch (actionOfSolarCar) {
                    case 1:
                        counterOfMaxSpeed++;
                        speedLimit(counterOfMaxSpeed);
                        counterWheels--;
                        fuel = fuel - 0.08;
                        break;
                    case 2:
                        counterOfMaxSpeed--;
                        speedLimit(counterOfMaxSpeed);
                        counterWheels--;
                        fuel = fuel - 0.08;
                        break;
                    case 3:
                        turn();
                        counterWheels--;
                        fuel = fuel - 0.08;
                        break;
                    case 4:
                        System.out.println("Fuel: " + fuel + " liters. ");
                        break;
                    case 5:
                        addFuel();
                        break;
                    case 6:
                        defaultInformationAboutTheSolarCar();
                        break;
                    case 7:
                        fuel = fuel - 8;
                        counterWheels--;
                        System.out.println("You drove 100 km. ");
                        break;
                    case 8:
                        fuel = fuel + 10;
                        System.out.println("You have filled 10 liters. ");
                        break;
                    case 9:
                        fuel = fuel - 0.08;
                        counterOfMaxSpeed = counterOfMaxSpeed + 10;
                        counterWheels--;
                        System.out.println("You increased the speed of 10 km / h ");
                        speedLimit(counterOfMaxSpeed);
                        break;
                    case 10:
                        fuel = fuel - 0.08;
                        counterOfMaxSpeed = counterOfMaxSpeed - 10;
                        counterWheels--;
                        System.out.println("You lowered the speed to 10 km / h ");
                        break;
                    case 0:
                        exitSolarCar = brake();
                        break;
                    case 111:
                        info();
                        break;
                    default:
                        System.out.println(errorSpelling);
                        break;
                }
            } else if (counterWheels < 1) {
                replaceWheels();
            } else {
                addFuel();
            }
        } while (exitSolarCar != true);
    }


}
