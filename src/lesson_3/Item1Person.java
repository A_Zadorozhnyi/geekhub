package lesson_3;

/**
 * Created by Alex_B on 17.11.2014.
 */
public class Item1Person implements Comparable {

    String lastName;
    int age;

    Item1Person(String lastName, int age) {
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        Item1Person person = (Item1Person) o;

        int result = lastName.compareTo(person.lastName);
        if (result != 0) {
            return result;
        }

        result = person.age;
        if (result != 0) {
            return  result ;
        }
        return 0;
    }

}
