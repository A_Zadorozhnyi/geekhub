package lesson_3;

/**
 * Created by Alex_B on 28.10.2014.
 */
public class Item3Engine {

    Item3Wheels wheels = new Item3Wheels();
    Item3GasTank gasTank = new Item3GasTank();

    public boolean engine(int counterWheels, double fuel) {
        if ((wheels.wheels(counterWheels) == true) && (gasTank.gasTank(fuel) == true)) {
            return true;
        } else {
            return false;
        }
    }
}
