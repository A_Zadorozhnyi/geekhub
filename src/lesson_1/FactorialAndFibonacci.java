package lesson_1;

import java.util.Scanner;

public class FactorialAndFibonacci {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        int action;
        String messageHeadPanel = "To calculate the factorial enter 1" +
                "\nTo calculate the Fibonacci numbers, enter 2" +
                "\nTo derive the sequence of Fibonacci numbers, enter 3" +
                "\nto print a number in the form of words, enter 4, 5 or 6" +
                "\nIf you want exit, enter 0" +
                "\nInput field ↓";

        while (true) {
            System.out.println(messageHeadPanel);
            action = scanner.nextInt();
            System.out.println("You have entered " + action);
            Logic logic = new Logic();
            if (action == 1) {
                logic.factorial();
            } else if (action == 2) {
                logic.fibonacci();
            } else if (action == 3) {
                logic.fibonacciNumbers();
            } else if (action == 4) {
                logic.outputInTheFormOfWords();
            } else if (action == 5) {
                logic.outputInTheFormOfWords2();
            } else if (action == 6) {
                logic.outputInTheFormOfWords3();
            } else if (action == 0) {
                System.out.println("---");
                System.out.println("Exit");
                System.out.println("---");
                break;
            }
        }
    }
}

