package lesson_1;

/**
 * Created by A_Zadorozhnyi on 21.10.2014.
 */
public interface Action {

    void factorial();

    void fibonacci();

    void fibonacciNumbers();

    void outputInTheFormOfWords();

    void outputInTheFormOfWords2();

    void outputInTheFormOfWords3();

}
