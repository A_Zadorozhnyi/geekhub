package lesson_1;

import java.util.Scanner;

/**
 * Created by A_Zadorozhnyi on 21.10.2014.
 */
public class Logic implements Action {

    Scanner scanner = new Scanner(System.in);
    String errorMinus = "Error! You entered minus values. Try again.";
    String errorTooHighValue = "Error! You have entered too much value to calculate the factorial." +
            "\nThe maximum allowable value = 31. Try again.";
    String errorTryAgain = "Error! You entered minus values. Try again.";


    public static int calculationOfTheFactorial(int fact) {
        if (fact == 0) return 1;
        return fact * calculationOfTheFactorial(fact - 1);

    }

    public static int calculationOfTheFibonacci(int fib) {
        if (fib == 0) return 0;
        if (fib == 1) return 1;
        return calculationOfTheFibonacci(fib - 1) + calculationOfTheFibonacci(fib - 2);
    }

    @Override
    public void factorial() {
        System.out.println("---");
        System.out.println("Enter the number to calculate its factorial");
        if (scanner.hasNextInt()) {
            int valueOfFactorial = scanner.nextInt();
            if (valueOfFactorial >= 32) {
                System.out.println(errorTooHighValue);
            } else if (valueOfFactorial < 0) {
                System.out.println(errorMinus);
            } else {
                System.out.println("factorial = " + calculationOfTheFactorial(valueOfFactorial));
            }
        } else {
            System.out.println(errorTryAgain);
        }
        System.out.println("---");
    }

    @Override
    public void fibonacci() {
        System.out.println("---");
        System.out.println("Enter fibonacci ");
        int fibonacciNumber = scanner.nextInt();
        if (fibonacciNumber >= 0) {
            System.out.println("fibonacci = " + calculationOfTheFibonacci(fibonacciNumber));
        } else {
            System.out.println(errorMinus);
        }
        System.out.println("---");
    }

    @Override
    public void fibonacciNumbers() {
        System.out.println("---");
        System.out.println("Enter fibonacci ");
        int fibonacciNumbers = scanner.nextInt();
        if (fibonacciNumbers >= 0) {
            int array[] = new int[fibonacciNumbers];
            for (int j = 0; j < fibonacciNumbers; j++) {
                array[j] = calculationOfTheFibonacci(j);
                System.out.println(array[j]);
            }
        } else {
            System.out.println(errorMinus);
        }
        System.out.println("---");
    }

    @Override
    public void outputInTheFormOfWords() {
        System.out.println("---");
        System.out.println("Enter the number to be displayed in the form of words ");
        int scannerNumber = scanner.nextInt();
        String arrayOfWords[] = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
        switch (scannerNumber) {
            case 0:
                System.out.println(arrayOfWords[0]);
                break;
            case 1:
                System.out.println(arrayOfWords[1]);
                break;
            case 2:
                System.out.println(arrayOfWords[2]);
                break;
            case 3:
                System.out.println(arrayOfWords[3]);
                break;
            case 4:
                System.out.println(arrayOfWords[4]);
                break;
            case 5:
                System.out.println(arrayOfWords[5]);
                break;
            case 6:
                System.out.println(arrayOfWords[6]);
                break;
            case 7:
                System.out.println(arrayOfWords[7]);
                break;
            case 8:
                System.out.println(arrayOfWords[8]);
                break;
            case 9:
                System.out.println(arrayOfWords[9]);
                break;
        }
        System.out.println("---");
    }

    @Override
    public void outputInTheFormOfWords2() {
        System.out.println("---");
        System.out.println("Enter the number to be displayed in the form of words ");
        int scannerNumber = scanner.nextInt();
                if (scannerNumber == 0){
                    System.out.println("Zero");
                } else if (scannerNumber == 1){
                System.out.println("One");
                } else if (scannerNumber == 2){
                System.out.println("Two");
                } else if (scannerNumber == 3){
                System.out.println("Three");
                } else if (scannerNumber == 4){
                System.out.println("Four");
                } else if (scannerNumber == 5){
                System.out.println("Five");
                } else if (scannerNumber == 6){
                System.out.println("Six");
                } else if (scannerNumber == 7){
                System.out.println("Seven");
                } else if (scannerNumber == 8){
                System.out.println("Eight");
                } else if (scannerNumber == 9) {
                    System.out.println("Nine");
                }
        System.out.println("---");
    }

    @Override
    public void outputInTheFormOfWords3() {
        System.out.println("---");
        System.out.println("Enter the number to be displayed in the form of words ");
        int scannerNumber = scanner.nextInt();
        String arrayOfWords[] = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
                System.out.println(arrayOfWords[scannerNumber]);
        System.out.println("---");
    }
}

