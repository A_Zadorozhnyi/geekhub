package lesson_2;

import java.util.Scanner;

/**
 * Created by A_Zadorozhnyi on 23.10.2014.
 */
public class SolarCar extends Vehicle {

    Engine engine = new Engine();
    Scanner scanner = new Scanner(System.in);
    String menuOfPlesureBoat = "1 - accelerate. \n2 - turn. " +
            "\n3 - check the amount of fuel. \n4 - add fuel. " +
            "\n0 - return. ";
    boolean exitSolarCar = false;
    String errorTankCapacity = "Error, tank capacity is 30 liters";
    int counterWheels = 5;
    int fuel = 6;

    public void landTransport() {
        System.out.println("It's a ground transportation");
    }

    public void replaceWheels() {
        System.out.println("Wheels are worn!\n1 - to change them. \n2 - to exit. ");
        int replaceWheels = scanner.nextInt();
        if (replaceWheels == 1) {
            counterWheels = 5;
        } else {
            System.out.println("Wheels burst and you have an accident.");
            exitSolarCar = brake();
        }
    }

    public void addFuel() {
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("You can even fill " + (30 - fuel) + " liter. ");
        int addFuel = scanner.nextInt();
        if ((addFuel + fuel) <= 30) {
            fuel += addFuel;
        } else {
            System.out.println(errorTankCapacity);
        }
    }

    public void functional() {
        landTransport();
        do {
            if (engine.engine(counterWheels, fuel) == true) {
                System.out.println(menuOfPlesureBoat);
                int actionOfSolarCar = scanner.nextInt();
                switch (actionOfSolarCar) {
                    case 1:
                        accelerate();
                        counterWheels--;
                        fuel = fuel - 2;
                        break;
                    case 2:
                        turn();
                        counterWheels--;
                        fuel = fuel - 2;
                        break;
                    case 3:
                        System.out.println("Fuel: " + fuel + " liters. ");
                        break;
                    case 4:
                        addFuel();
                        break;
                    case 0:
                        exitSolarCar = brake();
                        break;
                    default:
                        System.out.println(errorSpelling);
                        break;
                }
            } else if (counterWheels < 1) {
                replaceWheels();
            } else {
                addFuel();
            }
        } while (exitSolarCar != true);
    }


}
