package lesson_2;

import java.util.Scanner;

/**
 * Created by A_Zadorozhnyi on 23.10.2014.
 */
public class Boat extends Vehicle {

    Engine engine = new Engine();
    Scanner scanner = new Scanner(System.in);
    String errorTankCapacity = "Error, tank capacity is 20 liters";
    String menuOfPlesureBoat = "1 - accelerate. \n2 - turn. " +
            "\n3 - check the amount of fuel. \n4 - add fuel. " +
            "\n0 - return, enter 0. ";
    boolean exitBoat = false;
    int fuel = 20;
    int counterWheels = 1;

    public int gasTank(int fuel) {
        return fuel;
    }

    public void opportunityToSwim() {
        System.out.println("On this vehicle, you can swim");
    }

    public void addFuel() {
        System.out.println("In tank " + fuel + " liters. ");
        System.out.println("You can even fill " + (20 - fuel) + " liter. ");
        int addFuel = scanner.nextInt();
        if ((addFuel + fuel) <= 20) {
            fuel += addFuel;
        } else {
            System.out.println(errorTankCapacity);
        }
    }


    public void functional() {
        opportunityToSwim();
        gasTank(fuel);
        do {
            if (engine.engine(counterWheels, fuel) == true) {
                System.out.println(menuOfPlesureBoat);
                int actionOfBoat = scanner.nextInt();
                switch (actionOfBoat) {
                    case 1:
                        accelerate();
                        fuel--;
                        break;
                    case 2:
                        turn();
                        fuel--;
                        break;
                    case 3:
                        System.out.println("Fuel: " + gasTank(fuel) + " liters. ");
                        break;
                    case 4:
                        addFuel();
                        break;
                    case 0:
                        exitBoat = brake();
                        break;
                    default:
                        System.out.println(errorSpelling);
                        break;
                }
            } else {
                addFuel();
            }
        } while (exitBoat != true);
    }

}
