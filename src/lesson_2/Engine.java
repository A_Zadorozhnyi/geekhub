package lesson_2;

/**
 * Created by Alex_B on 28.10.2014.
 */
public class Engine {

    Wheels wheels = new Wheels();
    GasTank gasTank = new GasTank();

    public boolean engine(int counterWheels, int fuel) {
        if ((wheels.wheels(counterWheels) == true) && (gasTank.gasTank(fuel) == true)) {
            return true;
        } else {
            return false;
        }
    }
}
