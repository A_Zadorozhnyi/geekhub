package lesson_2;

/**
 * Created by Alex_B on 28.10.2014.
 */
public class GasTank {


    public boolean gasTank(int fuel) {
        if (fuel > 0) {
            return true;
        } else {
            System.out.println("Error! In the tank ran out of fuel ");
            return false;
        }
    }

}
