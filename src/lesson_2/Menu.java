package lesson_2;

import java.util.Scanner;

/**
 * Created by Alex_B on 22.10.2014.
 */
public class Menu {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        Boat boat = new Boat();
        SolarCar solarCar = new SolarCar();
        boolean exit = false;

        while (exit != true){
            System.out.println(" 1 - select boat \n 2 - select solar car \n 0 - exit");
            int selectCar = scanner.nextInt();
            switch (selectCar) {
                case 1:
                    boat.functional();
                    break;
                case 2:
                    solarCar.functional();
                    break;
                case 0:
                    exit = boat.brake();
                    break;
                default:
                    System.out.println("Error!");
            }
        }
    }

}
