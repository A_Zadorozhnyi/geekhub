package lesson_2;

import java.util.Scanner;

/**
 * Created by Alex_B on 27.10.2014.
 */
abstract class Vehicle implements Driveable {

    Scanner scanner = new Scanner(System.in);
    String turnLeft = "You turn left";
    String turnRight = "You turn right";
    String accelerate = "You accelerated";
    String errorSpelling = "Error, check your spelling";

    @Override
    public void accelerate() {
        System.out.println(accelerate);
    }

    @Override
    public boolean brake() {
        return true;
    }

    @Override
    public void turn() {
        System.out.println("1 - turn left. \n2 - turn right. ");
        if (scanner.hasNextInt()) {
            int direction = scanner.nextInt();
            switch (direction) {
                case 1:
                    System.out.println(turnLeft);
                    break;
                case 2:
                    System.out.println(turnRight);
                    break;
                default:
                    System.out.println(errorSpelling);
                    break;
            }
        } else {
            System.out.println("Error, you entered is not a number. ");
            turn();
        }
    }

}
