package lesson_2;

/**
 * Created by Alex_B on 22.10.2014.
 */
public interface Driveable {

    void accelerate();

    boolean brake();

    void turn();

}
