package lesson_9;

import java.io.File;

/**
 * Created by Alex_B on 23.01.2015.
 */
public class DirInfo {
    int itemNumber;
    File path;

    public DirInfo(int itemNumber, File path) {
        this.itemNumber = itemNumber;
        this.path = path;
    }

    public int getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    public File getPath() {
        return path;
    }

    public void setPath(File path) {
        this.path = path;
    }
}
