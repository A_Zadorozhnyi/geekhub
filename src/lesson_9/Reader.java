package lesson_9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Alex_B on 27.01.2015.
 */
public class Reader {
    public static String load(String pathToSource) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToSource));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
                stringBuilder.append("<br>");
            }
        } catch (IOException e) {
            System.out.println("IOException");
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
        return stringBuilder.toString();
    }
}
