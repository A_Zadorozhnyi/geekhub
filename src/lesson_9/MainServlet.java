package lesson_9;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex_B on 23.01.2015.
 */


@WebServlet("/")
public class MainServlet extends HttpServlet {

    Reader reader;
    Writer writer;
    CreateDelete createDelete;
    GetAbsolutePath getAbsolutePath;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("set".equals(request.getParameter("action"))) {
            String pathFile = request.getParameter("files");
            writer.write(pathFile, request.getParameter("readFile"));
            request.setAttribute("pathFile", pathFile);
            request.getRequestDispatcher("/main.jsp").forward(request, response);
        }
        if ("edit".equals(request.getParameter("action"))) {
            String pathFile = request.getParameter("files");
            request.setAttribute("pathFile", pathFile);
            request.getRequestDispatcher("/main.jsp").forward(request, response);
        }
        if ("create".equals(request.getParameter("action"))){
            String pathFile = request.getParameter("pathFile");
            createDelete.create(pathFile, request.getParameter("fileName"));
            request.setAttribute("files", getAbsolutePath.getAbsolutePath(pathFile));
            request.setAttribute("pathFile", pathFile);
            request.getRequestDispatcher("/main.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathFile;
        if (request.getParameter("action") == null) {
            pathFile = "D:/aa/";
        } else if ("parentDirectory".equals(request.getParameter("action"))) {
            pathFile = request.getParameter("files").substring(0, request.getParameter("files").lastIndexOf('\\'));
        } else {
            pathFile = request.getParameter("action");
        }
        if ("edit".equals(request.getParameter("action"))) {
            request.setAttribute("readFile", reader.load(pathFile));
            request.setAttribute("pathFile", request.getParameter("pathFile"));
            request.getRequestDispatcher("/edit.jsp").forward(request, response);
        }
        if ("create".equals(request.getParameter("action"))){
            pathFile = request.getParameter("pathFile");
            request.setAttribute("pathFile", pathFile);
            request.getRequestDispatcher("/createFile.jsp").forward(request, response);
        }
        if ("delete".equals(request.getParameter("action"))){
            pathFile = request.getParameter("pathFile");
            createDelete.delete(request.getParameter("fileToDel"));
        }
        if (pathFile.indexOf('.') == -1) {
            request.setAttribute("files", getAbsolutePath.getAbsolutePath(pathFile));
        } else {
            request.setAttribute("readFile", reader.load(pathFile));
        }

        request.setAttribute("pathFile", pathFile);
        request.getRequestDispatcher("/main.jsp").forward(request, response);

    }

}


