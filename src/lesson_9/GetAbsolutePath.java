package lesson_9;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex_B on 29.01.2015.
 */
public class GetAbsolutePath {

    public static File[] getAbsolutePath(String pathFile) throws IOException {
        File dir = new File(pathFile);
        File[] files = dir.listFiles();
        dir.getAbsolutePath();
        List<Dir> dirs = new LinkedList<Dir>();
        int i = 0;
        for (File f2 : files) {
            f2.getAbsolutePath();
            File path = files[0];
            dirs.add(createDir(i + 1, path));
        }
        return files;
    }

    public static Dir createDir(int itemNumber, File path) {
        return new Dir(itemNumber, path);
    }
}
