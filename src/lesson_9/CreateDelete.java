package lesson_9;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alex_B on 28.01.2015.
 */
public class CreateDelete {

    public static String create(String pathToSource, String fileName) throws IOException {
        File file = new File(pathToSource + "\\" + fileName);
            file.createNewFile();
            System.out.println("file.createNewFile");
            return "Create" + file.toString();
        }
    public static String delete(String pathToSource) throws IOException {
        File file = new File(pathToSource);
        file.delete();
            return "Delete" + file.toString();
        }
    }
