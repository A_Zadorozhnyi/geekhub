package lesson_9;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Alex_B on 27.01.2015.
 */
public class Writer {
    public static String write(String pathToSource, String text) throws IOException {
        PrintWriter printWriter = new PrintWriter(pathToSource);
        printWriter.write(text);
        printWriter.close();
        return printWriter.toString();
    }
}