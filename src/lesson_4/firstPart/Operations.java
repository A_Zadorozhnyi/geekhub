package lesson_4.firstPart;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alex_B on 03.12.2014.
 */
public class Operations implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {

        if (a.size() != b.size()) {
            return false;
        }
        if (a.containsAll(b) && b.containsAll(a)) {
            return true;
        }
        return false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set result = new HashSet();
        result.addAll(a);
        result.addAll(b);
        return result;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set result = new HashSet();
        result.addAll(a);
        result.removeAll(b);
        return result;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set result = new HashSet();
        result.addAll(a);
        result.retainAll(b);
        return result;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set result = new HashSet();
        result.addAll(a);
        Set resultSecondPart = new HashSet();
        resultSecondPart.addAll(b);
        result.removeAll(b);
        resultSecondPart.removeAll(a);
        result.addAll(resultSecondPart);
        return result;
    }
}
