package lesson_4.firstPart;

/**
 * Created by Alex_B on 04.12.2014.
 */

import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        Operations operations = new Operations();
        HashSet<Integer> a = new HashSet<Integer>();
        a.add(1);
        a.add(3);
        a.add(4);
        a.add(5);

        HashSet<Integer> b = new HashSet<Integer>();
        b.add(1);
        b.add(2);
        b.add(3);

        System.out.println("operations.equals");
        System.out.println(operations.equals(a, b));
        System.out.println("---");

        System.out.println("operations.intersect");
        HashSet<Integer> result = (HashSet) operations.intersect(a, b);
        for (Integer sortResult : result) {
            System.out.println(sortResult);
        }
        System.out.println("---");

        System.out.println("operations.union");
        result = (HashSet) operations.union(a, b);
        for (Integer sortResult : result) {
            System.out.println(sortResult);
        }
        System.out.println("---");

        System.out.println("operations.subtract");
        result = (HashSet) operations.subtract(a, b);
        for (Integer sortResult : result) {
            System.out.println(sortResult);
        }
        System.out.println("---");

        System.out.println("operations.symmetricSubtract");
        result = (HashSet) operations.symmetricSubtract(a, b);
        for (Integer sortResult : result) {
            System.out.println(sortResult);
        }
        System.out.println("---");

    }
}
