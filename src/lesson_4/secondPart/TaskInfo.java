package lesson_4.secondPart;

/**
 * Created by Alex_B on 22.01.2015.
 */
public class TaskInfo {

    Task info;

    public TaskInfo(String category, String description) {
        this.info = new Task(category, description);
    }

    public Task getInfo() {
        return info;
    }

    public void setInfo(Task info) {
        this.info = info;
    }
}
