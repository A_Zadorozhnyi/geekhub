package lesson_5;

import lesson_5.source.SourceLoader;
import lesson_5.source.URLSourceProvider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

// cw.1.1.20141123T082751Z.24e2acbe43dae57a.add40c5f214c9291e1a93ddf505aac57a5f9a595

/**
* Provides utilities for translating texts to russian language.<br/>
* Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
* Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
*/
public class Translator {
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "{cw.1.1.20141123T082751Z.24e2acbe43dae57a.add40c5f214c9291e1a93ddf505aac57a5f9a595}";
    private static final String TRANSLATION_DIRECTION = "ru";

    SourceLoader sourceLoader = new SourceLoader();

    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws java.io.IOException
     */
    public String translate(String original) throws IOException {
        //TODO: implement me
        String text = parseContent(original);
        return text;
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text
     */
    private String prepareURL(String text) throws UnsupportedEncodingException {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) throws IOException {
        //TODO: implement me
       String text = prepareURL(content);
        return text;
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) throws UnsupportedEncodingException {
        //TODO: implement me
        String url = URLEncoder.encode(text, "UTF-8");
        return url;
    }
}
