package lesson_5.source;

import java.io.*;

import static java.nio.file.Files.exists;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        pathToSource = "d:/test.txt";
        boolean result;
        File file = new File(pathToSource);
        if (file.exists()) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
//        pathToSource = "d:/test.txt";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToSource));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
        } catch (IOException e) {
            System.out.println("IOException");
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
        return stringBuilder.toString();
    }
}
