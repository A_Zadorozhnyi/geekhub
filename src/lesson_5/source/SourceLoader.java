package lesson_5.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    FileSourceProvider fileSourceProvider = new FileSourceProvider();
    URLSourceProvider urlSourceProvider = new URLSourceProvider();


    public SourceLoader() {
        //TODO: initialize me
        sourceProviders.add(fileSourceProvider);
//        sourceProviders.add(urlSourceProvider);


//        this.sourceProviders = sourceProviders;
//        return sourceProviders;
    }

//    public SourceLoader(List<SourceProvider> sourceProviders) {
//        this.sourceProviders = sourceProviders;
//    }

    public String loadSource(String pathToSource) throws IOException {
        //TODO: implement me
        String loadSourcePrivider;
        if (fileSourceProvider.isAllowed(pathToSource) != false) {
            loadSourcePrivider = fileSourceProvider.load(pathToSource);
        } else {
            loadSourcePrivider = fileSourceProvider.load(pathToSource);
        }
        return loadSourcePrivider;
    }
}
