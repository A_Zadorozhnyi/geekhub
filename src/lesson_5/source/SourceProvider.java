package lesson_5.source;

import java.io.IOException;

/**
 * Base interface to access different sources.</br>
 * isAllowed method should protect and help to determine can we load resource for specified path or* not.
 * <p/>
 * Базовый интерфейс для доступа к различным источникам. </ BR>
 * IsAllowed метод должен защищать и помогать, чтобы определить, можем ли мы загрузить ресурс для указанного пути или * нет.
 */
public interface SourceProvider {
    /**
     * Determines can current implementation load source by provided pathToSource
     *
     * @param pathToSource absolute path to the source
     * @return whether current implementation load the source for specified pathToSource
     * <p/>
     * Определяет, может ли реализация pathToSource подгрузить ресурс
     *
     * @param pathToSource абсолютный путь к источнику
     * @return где текущая реализация загрузки ресурса для заданной pathToSource
     */
    public boolean isAllowed(String pathToSource);

    /**
     * Loads text from specified path.
     *
     * @param pathToSource absolute path to the source
     * @return content of the source for specified pathToSource
     */
    public String load(String pathToSource) throws IOException;
}
