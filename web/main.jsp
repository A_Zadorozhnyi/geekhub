<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>User</title>
</head>
<body>
</br></br></br>
<table>
    <h1><b> Current path: ${pathFile}</b></h1>
    <br>
    <a href="/?action=parentDirectory&files=${pathFile}">Parent directory </a>
    <c:set var="i" value="${0}"/>
    <c:forEach var="files" items="${files}" varStatus="filesCounter">
        <c:if test="${filesCounter.count mod 2 == 1}">
            <tr style="background-color: white;" >
        </c:if>
        <c:if test="${filesCounter.count mod 2 == 0}">
            <tr style="background-color: rgb(220,220,240);" >
        </c:if>
        <c:set var="i" value="${ i+1 }"/>
        <td>
            <c:out value="${filesCounter.count}"/>
        </td>
        <td>
            <a href="/?action=${files.getAbsolutePath()}&files=${files.getAbsolutePath()}"> <c:out value="${files.getAbsolutePath()}"/></a>
            <a href="/?action=delete&fileToDel=${files.getAbsolutePath()}&pathFile=${pathFile}">(del)</a></br>
        </td>

        </tr>
    </c:forEach>
    <br>
    <a href="/?action=edit&files=${readFile}&pathFile=${pathFile}">Edit</a></br>
    ${readFile}
    <a href="/?action=create&pathFile=${pathFile}">Create file</a></br>
</table>
</body>
</html>