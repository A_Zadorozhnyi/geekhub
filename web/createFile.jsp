<%--
  Created by IntelliJ IDEA.
  User: Alex_B
  Date: 28.01.2015
  Time: 13:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<table>
    <h1><b> Current path: ${pathFile}</b></h1>
    <br>
    <a href="/?action=parentDirectory&files=${pathFile}">Parent directory </a>
    <br>
    <form action="/" method=post>
        <input type=hidden name=action value="create">
        <input type=hidden name=pathFile value="${pathFile}">
        Edit:  <input type=text name=fileName maxlength=30 value='New File.txt'>

        <input type=submit value='Create'>
    </form>
    <form action="/" method=get>
        <input type=submit value='Ignore'>
    </form>
</table>
</body>
</html>
