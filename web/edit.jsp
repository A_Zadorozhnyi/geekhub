<%--
  Created by IntelliJ IDEA.
  User: Alex_B
  Date: 27.01.2015
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit</title>
</head>
<body>
</br></br></br>
<table>
    <h1><b> Current path: ${pathFile}</b></h1>
    <br>
    <a href="/?action=parentDirectory&files=${pathFile}">Parent directory </a>
    <br>
    <form action="/" method=post>
        <input type=hidden name=action value="set">
        <input type=hidden name=files value="${pathFile}">
        Edit:  <input type=text name=readFile maxlength=999 value='${readFile}'>

        <input type=submit value='Save'>
    </form>
    <form action="/" method=get>
        <%--<input type=hidden name=action value="view">--%>
        <input type=submit value='Ignore'>
    </form>
</table>
</body>
</html>
